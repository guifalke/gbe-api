﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Model;

namespace DB
{
    public class LogRepo:DbContext  
    {
        public DbSet<Logger> Logger { get; set; }
        public LogRepo(DbContextOptions options) : base(options)
        {

        }
        public async Task insert(Logger toInsert)
        {
            try
            {
                await Logger.AddAsync(toInsert);
                await this.SaveChangesAsync();
            }
            catch (Exception e) {
                throw;
            }


        }
        public async Task<Logger[]> getAll()
        {
            return await Logger.ToArrayAsync();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
