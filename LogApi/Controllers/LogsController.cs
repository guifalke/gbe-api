﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DB;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Model;
using Newtonsoft.Json;

namespace LogApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [EnableCors("all")]
    public class LogsController : ControllerBase
    {
        private readonly LogRepo _logRepo;        
        public LogsController(LogRepo logRepo) {
            _logRepo = logRepo;
        }
        [HttpGet]
        public async  Task<ActionResult<string>> Get()
        {
            //return "ready";
            Logger[] logs = await _logRepo.getAll();
            return JsonConvert.SerializeObject(logs);
        }

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody] Logger value)
        {
            await _logRepo.insert(value);
        }

    }
}
