﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Logger
    {
        [Key]
        public int Log_ID { get; set; }
        public string AplicationName { get; set; }
        public DateTime Timestamp { get; set; }
        public int Severity { get; set; }
        public string LogMessage { get; set; }

    }
}
