﻿using Model;
using System;
using System.Threading.Tasks;
using DB;

namespace Service
{
    public class LogService
    {
        private readonly LogRepo _LogRepo;
        public LogService(LogRepo LogRepo)
        {
            _LogRepo = LogRepo;
        }
        public Task insert(Log toInsert)
        {
            return _LogRepo.insert(toInsert);
        }
    }
}
